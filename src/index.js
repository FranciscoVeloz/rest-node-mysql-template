const express = require('express')
const morgan = require('morgan')
const cors = require('cors')

//Importing routes
const indexRoutes = require('./routes/indexRoutes')

class Server {
    constructor() {
        this.app = express()
        this.Settings()
        this.Config()
        this.Routes()
        this.Start()
    }

    Settings() {
        this.app.set('port', process.env.PORT || 3000)
    }

    Config() {
        this.app.use(morgan('dev'))
        this.app.use(cors())
        this.app.use(express.json())
        this.app.use(express.urlencoded({extended: false}))
    }

    Routes() {
        this.app.use('/', indexRoutes)
    }

    Start() {
        this.app.listen(this.app.get('port'), () => {
            console.log(`Server on port ${this.app.get('port')}`)
        })
    }
}

const server = new Server()