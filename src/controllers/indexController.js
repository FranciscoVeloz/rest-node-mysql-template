const pool = require('../database')

class IndexController {

    async List(req, res) {
        try {
            const data = await pool.query('select * from table')
            res.json(data)
        } catch (error) {
            console.error(error)
        }
    }

    async GetOne(req, res) {
        const { id } = req.params

        try {
            const data = await pool.query('select * from table where id = ?', [id])

            if (data.length > 0) {
                res.json(data[0])
            } else {
                res.status(404).json({ text: "El dato no existe" })
            }
        } catch (error) {
            console.error(error)
        }
    }

    async Insert(req, res) {
        try {
            const result = await pool.query('insert into table set ?', [req.body])
            res.json({ message: 'Dato insertado' })
        } catch (error) {
            console.error(error)
        }
    }

    async Update(req, res) {
        const { id } = req.params
        const oldData = req.body
        try {
            await pool.query('update table set ? where id = ?', [oldData, id])
            res.json({ message: "El dato fue actualizado" })
        } catch (error) {
            console.error(error)
        }
    }

    async Delete(req, res) {
        const { id } = req.params
        try {
            await pool.query('delete from table where id = ?', [id])
            res.json({message: "El dato fue eliminado"})
        } catch (error) {
            console.error(error)
        }
    }
}

module.exports = new IndexController()