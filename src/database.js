const mysql = require('mysql')
const { promisify } = require('util')

//Importing database keys
const keys = require('./keys')

//Creating mysql pool connection
const pool = mysql.createPool(keys.database)

pool.getConnection((err, connection) => {
    if(err) {
        console.error('Error in the DB connection')
    }

    if(connection) connection.release()
    console.log('DB is connected')

    return
})

//Promisify pool querys
pool.query = promisify(pool.query)

module.exports = pool