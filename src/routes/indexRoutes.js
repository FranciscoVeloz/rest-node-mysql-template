const { Router } = require('express')
const controller = require('../controllers/indexController')

class IndexRoutes {
    constructor() {
        this.router = Router()
        this.Config()
    }

    Config() {
        this.router.get('/', controller.List)
        this.router.get('/:id', controller.GetOne)
        this.router.post('/', controller.Insert)
        this.router.put('/:id', controller.Update)
        this.router.delete('/:id', controller.Delete)
    }
}

module.exports = new IndexRoutes().router